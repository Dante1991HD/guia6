#ifndef DIJKSTRA_H
#define DIJKSTRA_H

//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

class Dijkstra {
    private:
        
    public:
        //Constructor
        Dijkstra();
        
        // INicializadores
        void inicializar_vector_caracter (string *vector, int n);
        void inicializar_matriz_enteros(int **matriz, int n);
        void inicializar_vector_D(int *D, int **matriz, int n);

        // IMpresiones
        void imprimir_vector_caracter(string *vector, int n);  
        void imprimir_matriz(int **matriz, int n);
        void imprimir_vector_entero(int *vector, int n);
        
        // INgreso de datos a al matriz
        void agregar_nombres(int n, string nombres[], int **matriz);
        void llenar_matriz(int n, string nombres[], int **matriz);

        // FUnciones para el algoritmo
        void agrega_vertice_a_S(string *S, string vertice, int n); // vertice string
        void actualizar_VS(string *V, string *S, string *VS, int n);
        int busca_caracter(string c, string *vector, int n);
        string elegir_vertice(string *VS, int *D, string *V, int n); // vertice de string a int
        int buscar_indice_caracter(string *V, string caracter, int n);
        void actualizar_pesos(int *D, string *VS, int **matriz, string *V, string v, int n);
        int calcular_minimo(int dw, int dv, int mvw);

        // Algoritmo
        void aplicar_dijkstra(string *V, string *S, string *VS, int *D, int **matriz, int n);

        // Creacion e impresion del grafo (.png)
        void imprimir_grafo(int **matriz, string *vector, int n);

};
#endif