//Librerias
#include <fstream>
#include <iostream>
#include "Dijkstra.h"

using namespace std;

/*Constructor por defecto*/

Dijkstra::Dijkstra() {}


/* Inicializa un vector de tipo string */
void Dijkstra::inicializar_vector_caracter(string *vector, int n) {
    int col;
      for (col=0; col<n; col++) {
        vector[col] = ' ';
    }
}

/* Inicializa matriz de tamaño nxn */
void Dijkstra::inicializar_matriz_enteros(int **matriz, int n) {
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            matriz[fila][col] = -1;
        }
    }
}
/* Inicializa un vector de tipo int */
void Dijkstra::inicializar_vector_D(int *D, int **matriz, int n) {
    int col;
    for (col=0; col<n; col++) {
        D[col] = matriz[0][col];
    }
}

/* Imprime el vector de caracteres */
void Dijkstra::imprimir_vector_caracter(string *vector, int n) {
    for (int i=0; i<n; i++) {
        cout << "vector[" << i << "]: " << vector[i] << endl;
    }
}

/* Imprime la matriz de distancias */
void Dijkstra::imprimir_matriz(int **matriz, int n) {
    cout << "\t";
        for (int fila=0; fila<n; fila++) {
        cout << fila << "\t";
}
    cout << endl;
    for (int fila=0; fila<n; fila++) {
        cout << fila;
        for (int col=0; col<n; col++) {
            cout << "\t{" << matriz[fila][col] << "}" << " "; 
        }
        cout << endl;
    }
    cout << endl;
}

/* Imprime el vector de enteros */
void Dijkstra::imprimir_vector_entero(int *vector, int n){
    int i;
    for (i=0; i<n; i++) {
        cout << "D["<< i <<"]: " << vector[i] << endl;
    }
    
}

/* Asocia nombres a los nodos, los ingresa el usuario */
void Dijkstra::agregar_nombres(int n, string nombres[], int **matriz) {
    string nombre;
    cout << endl;
    for(int i=0; i<n; i++){
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nombres[i] = nombre;
    }
}

/* Asigna valores a la matriz de dimensiones designadas por el usuario */
void Dijkstra::llenar_matriz(int n, string nombres[], int **matriz){ 
    int distancia;
    /* Llenar la matriz */
    for (int i=0; i<n; i++){
        cout << "Distancias del nodo: " << nombres[i];
        cout << endl;
        
        for (int j=0; j<n; j++){
            if(i != j){
                cout << "Ingrese la distancia al nodo " << nombres[j] << ": ";
                cin >> distancia;

                matriz[i][j] = distancia;
            }
            else{
                /* Diagonal de la matriz*/
                matriz[i][j] = 0;
            }
        }
    }
}
/* Al encontrar un espacio en blanco lo iguala a vertice */
void Dijkstra::agrega_vertice_a_S(string *S, string vertice, int n){
    
    /* Se recorre buscando el espacio en blanco */
    for (int i=0; i<n; i++) {
        if (S[i] == " ") {
            S[i] = vertice;
            return;
        }
    } 
}

/* Al encontrar un valor faltante de V[] en S[], de encontrarlo lo agrega a VS[] */
void Dijkstra::actualizar_VS(string *V, string *S, string *VS, int n) {
    int k = 0;
    inicializar_vector_caracter(VS, n);
    /* Recorre buscando valores faltantes de V[] en S[] */
    for (int j=0; j<n; j++){
        if (busca_caracter(V[j], S, n) != true) {
            /* Al encontrarlo lo agrega a VS[] */
            VS[k] = V[j];
            k++;
        }
    }
}
/* Método utilitario de "actualizar_VS" buscará si el caracter se encuentra, 
    retorna un buleano en consecuencia */
int Dijkstra::busca_caracter(string c, string *vector, int n) {
    for (int j=0; j<n; j++) {
        if (c == vector[j]) {
        return true;
        }
    }
    return false;
}
/* Retorna el vercite según encuentra el de menor peso */
string Dijkstra::elegir_vertice(string *VS, int *D, string *V, int n){
    int i = 0;
    int menor = 0;
    int peso;
    string vertice;

    while (VS[i] != " ") {
        
        peso = D[buscar_indice_caracter(V, VS[i], n)];
        /* Descarta valores infinitos, -1 y 0 */
        if ((peso != -1) && (peso != 0)) {
            /* La primera iteración asigna directamente el peso al menor
            y vertice se asigna VS[actual] */
            if (i == 0) {
                menor = peso;
                vertice = VS[i];
            } 
            /* De lo contrario verificará si el peso es inferior a menor,
            de ser así se asignará peso a menor y nuevamente 
            a vertice se asigna VS[actual] */
            else {
                if (peso < menor) {
                    menor = peso;
                    vertice = VS[i];
                }
            }
        }
        i++;
    } 
    cout << "Vertice menor: " << vertice << endl;
    return vertice;
}

/* Método que retorna el indice en el que aparece dentro del vector V[] */
int Dijkstra::buscar_indice_caracter(string *V, string caracter, int n) {
    int i;
    /* recorre el vector con el caracter buscado para encontrar el indice */
    for (i=0; i<n; i++) {
        if (V[i] == caracter)
        return i;
    }
    return i;
}

/* Modifica los pesos relativos entre los nodos, se vale tanto de "buscar_indice_caracter"
como de "calcular_minimo" para establecer el nuevo peso */
void Dijkstra::actualizar_pesos(int *D, string *VS, int **matriz, string *V, string v, int n) {
    int i = 0;
    int indice_w, indice_v;
    cout << "\n> actualiza pesos en D[]" << endl;
    indice_v = buscar_indice_caracter(V, v, n);
    while (VS[i] != " ") {
        if (VS[i] != v) {
            indice_w = buscar_indice_caracter(V, VS[i], n);
            D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], matriz[indice_v][indice_w]);
            }
        i++;
    }
}
/* Retorna el minimo del vector D, utiliza los indices extraidos en 
método "actualizar_pesos" y el punto de estos indices en la matriz */
int Dijkstra::calcular_minimo(int dw, int dv, int mvw) {
    int min = 0;
    if (dw == -1) {
        if (dv != -1 && mvw != -1)
            min = dv + mvw;
        else
            min = -1;
    } else {
        if (dv != -1 && mvw != -1) {
            if (dw <= (dv + mvw)) {
                min = dw;
            } else {
                min = (dv + mvw);
            }
        } else {
            min = dw;
        }
    }
    cout << "\ndw: " << dw << "\ndv: " << dv << "\nmvw: " << mvw << "\nmin: " << min << endl;
    return min;
}

/* Se aplica el algorítmo de Dijstra */
void Dijkstra::aplicar_dijkstra(string *V, string *S, string *VS, int *D, int **matriz, int n){
    string v;
    
    /* inicializar vector D[] segun datos del puntero a "matriz" */
    cout << "\n> Se inicializa el vector D" << endl;
    inicializar_vector_D(D, matriz, n);
    cout << endl;
    imprimir_vector_entero(D, n);

    /* Se genera una salida con los datos entes de dar inicio al algoritmo */
    cout << "\n> Impresión de estados iniciales" << endl;
    cout << "Vector V: " << endl;
    imprimir_vector_caracter(V, n);
    cout << "Vector S: " << endl;
    imprimir_vector_caracter(S, n);
    cout << "Vector VS: " << endl;
    imprimir_vector_caracter(VS, n); 

    /* Se agrega el primer vertice y se actualiza VS[] */
    cout << "\n> Se agrega el primer valor V[0] a S[]" << endl;
    agrega_vertice_a_S(S, V[0], n);
    cout << "Vector S: " << endl;
    imprimir_vector_caracter(S, n);
    cout << "\n> Se actualiza VS[]" << endl;
    actualizar_VS(V, S, VS, n);
    cout << "Vector VS: " << endl;
    imprimir_vector_caracter(VS, n);
    cout << "\nVector D: " << endl;
    imprimir_vector_entero(D, n);
    /* Se elige el vertice menor y de manera iterativa se van agregando, además
    de actualizarse tanto los peso como VS */
    for (int i=1; i<n; i++) {
        cout << "\n> Se elige el vertice menor en VS[] según valores en D[]" << endl;
        cout << ">lo agrega a S[] y actualiza VS[]" << endl;
        v = elegir_vertice(VS, D, V, n);

        agrega_vertice_a_S(S, v, n);
        imprimir_vector_caracter(S, n);

        actualizar_VS(V, S, VS, n);
        imprimir_vector_caracter(VS, n);

        actualizar_pesos(D, VS, matriz, V, v, n);
        imprimir_vector_entero(D, n);
    }
    cout << endl;

} 


/* Metodo que genera un grafo con los datos generados con el método "aplicar_dijkstra" */

void Dijkstra::imprimir_grafo(int **matriz, string *vector, int n) {

    ofstream archivo("grafo.txt", ios::out);
    archivo << "digraph G {" << endl;
    archivo << "graph [rankdir=LR]" << endl;
    archivo << "node [style=filled fillcolor=purple]" << endl;
    /* Recorrerá la matriz e ingresará datos al archivo grafo.txt, de manera de formatear
    el grafo de la manera que buscamos */
    for (int i=0; i<n; i++) {        
        for (int j=0; j<n; j++) {
            if (i != j) {
                if (matriz[i][j] > 0) {
                    archivo << vector[i] << "->" << vector[j] << "[label=" + to_string(matriz[i][j]) << "]" << endl;

                }
            }
        }
    }
    
    /* Finalmente cierra el archivo generado */
    archivo << "}";
    archivo.close();

    /* Se utiliza system para que al ejecutar el programa se abra el archivo .png */
    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
} 
