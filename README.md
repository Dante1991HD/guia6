# Guía VI UII

## Descripción del programa 
El programa permite la generación de un grafo dado una matriz de distancias entre nodos, más de dos nodos minimo, con estos datos son tratados con el algoritmo de Dijktra entregando finalmente una imagen llamada "grafo.png" que contrendrá la reducción de la información a modo facil de entender, mostrará la menor distancia entre los nodos.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make
* Software Graphviz
En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.
En el caso de no contar con Graphviz

### ¿Como instalar graphviz?
Para instalar graphviz, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install graphviz
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el paquete. Una vez termine la descarga, el paquete estará listo para usar

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa #Número entero#
```
* Es de vital importancia que se agrege un numero entero desde la consola y que este sea mayor a 2 como en el siguiente ejemplo.
```
./programa 5
```
* luego pedirá la distancia entre los nodos uno a uno, para finalmente generar una salida de la siguiente forma

## PNG de salida
![Caption for the picture.](https://gitlab.com/Dante1991HD/guia6/-/raw/main/grafo.png)

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
